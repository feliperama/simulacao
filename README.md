# Simulacao restauracao

Lista de variaveis importantes

  * fds = Array de falhas duplas, lido do arquivo
  * opt_fds = idem fds, porem desconsidera sites que não são os escolhidos para regeneracao
  * nregens = Numero de vertices para serem regeneradores para a restauracao
  * vregens = vertices escolhidos para serem regeneradores baseado na heuristica dos nregens mais utilizados
  * dm = Hash com as demandas { id => value }
  * links_weight = array de links lidos do txt com a distancia configurada correto.
  * ll = contem os links e o throughput necessário seguindo heuristica da falha dupla - all regen 
  * opt_ll = contem os links e o throughput necessário seguindo heuristica da falha dupla - otimizado 
