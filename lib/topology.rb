class Link
  attr_accessor :src
  attr_accessor :dst
  attr_accessor :weight
  def initialize v1, v2, weight
    @src = v1
    @dst = v2
    @weight = weight
  end

  def ==(other)
    ((@src == other.src) && (@dst == other.dst)) ||
      ((@src == other.dst) && (@dst == other.src))
  end

  def to_s
    format "%s:%s", @src, @dst
  end

  def to_s_inverse
    format "%s:%s", @dst, @src
  end

  def get_neighbor_name v
    if v == @src
      return @dst
    elsif v == @dst
      return @src
    else
      return nil  #colocar uma exceção aqui
    end
  end

  def get_neighbor_weight v
    if v == @src || v ==@dst
      return @weight 
    else
      return nil  #colocar uma exceção aqui
    end
  end
end

class Link_list_counter
  attr_reader :links
  attr_reader :interfaces
  attr_reader :bandwidth
  attr_reader :v_ifs

  def initialize 
    @links = Hash.new { [].freeze }
    @interfaces = 0
    @bandwidth = 0
    @v_ifs = Hash.new
  end

  def calculate df_list, bandwidth, dm
    @interfaces = interfaces
    @bandwidth = bandwidth
    
    df_list.each do |df|
      aux_ll = Link_list_counter.new
      df.flows.each do |flow_id, rpath|
        rpath.each do |link|
          aux_ll.add_link link, (dm[flow_id])
        end
      end
      self << aux_ll
    end
    
    #contagem de interfaces total
    total = 0
    self.links.each do |link, count|
      if (count % bandwidth) > 0
        total = total + (((count / bandwidth) + 1) * 2)
      else
        total = total + ((count / bandwidth) * 2)
      end
    end
    @interfaces = total

    #contagem de interfaces por vertice

    @links.each do |lname, lbd|
      str = lname.split(':')
        @v_ifs[str[0]] = @v_ifs[str[0]].to_i + (calc_bd_ifs(lbd, bandwidth))
        @v_ifs[str[1]] = @v_ifs[str[1]].to_i + (calc_bd_ifs(lbd, bandwidth))
    end
      

  end

  def has_link link
    str = link.to_s
    istr = link.to_s_inverse

    if (@links[str]!=[] || @links[istr]!=[])
      true
    else
      false
    end
  end

  def get_key link
    get key_str
  end

  def add_link link, bandwidth
    str = link.to_s
    istr = link.to_s_inverse

    if @links[str]!=[]
      @links[str] = @links[str] + bandwidth
    elsif @links[istr]!=[]
      @links[istr] = @links[istr] + bandwidth
    else
      @links[str] = bandwidth
    end
  end


  def <<(other)
    other.links.each do |okey, ovalue|
      aux = okey.split(':')
      iokey = aux[1] << ":" << aux[0]
      if self.links[okey]!=[]
        if ovalue > self.links[okey]
          self.links[okey] = ovalue
        end
      elsif self.links[iokey]!=[]
        if ovalue > self.links[iokey]
          self.links[iokey] = ovalue
        end
      else
        self.links[okey] = ovalue
      end
    end
  end

  def to_s
    @links.to_s
  end

  private

  def calc_bd_ifs value, bd
    if (value % bd) == 0
      return (value / bd)
    else
      return ((value / bd) + 1)
    end
  end
end


class DoubleFault
  attr_accessor :link1            #Link1 da falha
  attr_accessor :link2            #Link2 da falha
  attr_accessor :flows            #Hash com id da demanda --> caminho restaurado para
                                # esta falha dupla
  attr_accessor :flows_v          #idem anterior, porem lista de vertices

  def initialize link1, link2
    @link1 = link1
    @link2 = link2
    @flows = Hash.new { [].freeze }
    @flows_v = Hash.new { [].freeze }
  end

  def to_s
    format @link1.to_s + " & " + @link2.to_s + " :" + @flows_v.to_s
  end

  def add_rflow flow_id, rpath, rpath_v
    @flows[flow_id] = rpath
    @flows_v[flow_id] = rpath_v
  end
end

class Regenerators
  attr_accessor :regs
  
  def initialize *args 

    case args.size
    when 1
      @regs = args[0]
    else
     # @regs = [ "FLA-ALD",
     #           "PI-TSI",
     #           "PE-SGI",
     #           "PC-EBVG",
     #           "SDR-BRRR",
     #           "SDR-BDEA",
     #           "BA-VCA",
     #           "BHE-HGA",
     #           "BHE-MPI",
     #           "VTA-ROS",
     #           "RJO-BOT",
     #           "MG-PTC",
     #           "MG-ULA",
     #           "SPO-GUA" ]
    
      #@regs = [ "FLA-ALD",
      #          "PI-TSI",
      #          "PE-SGI",
      #          "PC-EBVG",
      #          "SDR-BRRR"]

      @regs = ["RJO-BOT",
               "MG-ULA",
               "MG-PTC",
               "SPO-GUA",
               "PE-SGI",
               "RC-EBVG",
               "SDR-BRRR",
               "BA-VCA",
               "VTA-ROS",
               "BHE-HGA",
               "BHE-MPI",
               "SDR-BDEA",
               "SPO-BRS" ]

    end
  end


  def is_reg v
    self.regs.each do |r|
      if v == r
        return true
      end
    end
    return false
  end

  def to_s
    format "%s", @regs.to_s
  end
  
end















