require "./a_star.rb"

#Topology#

vl = ["a", "b", "c", "d", "e", "z" ]
ll = [ Link.new("a","b", 4),
       Link.new("a","c", 2),
       Link.new("b","c", 1),
       Link.new("b","d", 5),
       Link.new("c","d", 8),
       Link.new("c","e", 10),
       Link.new("d","e", 2),
       Link.new("d","z", 6),
       Link.new("e","z", 3) ]


vex = Dijkstra_vex.new :any
vex.nbs = [ll[0], ll[1]]
vex.d = 3.0
vex.prev = "d"


vex2 = vex.clone
vex2.d = 5.0
vex2.color = :B

teste = Graph.new ll, "a"

print "\n--------------------------------\n"
print " ITERATION LIST: #{ teste.iterations }"
print "\n--------------------------------\n"

path_list = teste.get_paths "z", 2

print "\n--------------------------------\n"
print "K paths: #{ path_list }"
print "\n--------------------------------\n\n"

#print "\n--------------------------------\n"
#teste.vlist.each do |vl|
## printf("0x%x\n", vl.object_id)
#  print "#{vl}\n\n"
#end
#
#print "\n#####################\n#{ teste.pqueue }\n\n"
