require "./topology.rb"

class Dijkstra_vex
  attr_accessor :nbs
  attr_accessor :d
  attr_accessor :prev
  attr_accessor :color

  def initialize type
    @nbs = Array.new
    if type == :source
      @d = 0.0
    else
      @d = Float::INFINITY
    end
    @prev = nil
    @color = :W
  end

  def clone
    new = Dijkstra_vex.new :source
    new.nbs   = []
    @nbs.each do |link|
      new.nbs.push link.clone
    end
    new.d     = @d
    new.prev  = @prev
    new.color = @color
    return new
  end
end

class Graph
  attr_reader :vlist          #Array of hashes with format {"node"=>Dijkstra vertices}
  attr_reader :pqueue         #Hash implementing the priority queueu of Dijkstra alg
  attr_reader :source_node    #Source node of graph
  attr_reader :iterations     #iterations where the vertice @d was changed  (indice for @vlist array), used by getpaths

  #The initialize function must running the Dijkstra alg
  def initialize links, src
    start_structures links, src
    
    count_it = 1          #iteration counter. Zero is the pointer to no iteration (start)
    while @pqueue.size > 1
      vl = vex_hash_clone @vlist.last
      p = @pqueue.min_by { |k, v| v }
      node = p[0]
      vl[node].color = :B
      
      #Djkstra iteration
      vl[node].nbs.each do |link|
        nb = link.get_neighbor_name node
        nb_weight = vl[node].d + link.weight
        
        if vl[nb].color == :B
          next
        end
        
        if vl[nb].d > nb_weight
          vl[nb].d = nb_weight
          @pqueue[nb] = nb_weight
          vl[nb].prev = node
          @iterations[nb] += [count_it]
        end
      end

      #save the last stage and decrement priority queue
      @vlist.push vl
      @pqueue.delete(node)
    
      count_it += 1  
    end

    #Unecessary code for color change, commented    
    #@vlist.last[pqueue.keys[0]].color = :B
  end

  #These function will returns the k best paths to a destination
  def get_paths dst, k
    path_list = []
    k_vlist = []
    max = @iterations[dst].size
    if k >= max 
      @iterations[dst].each do |iteration|
        k_vlist.push @vlist[iteration]
      end
    else
      its = @iterations[dst][max-k,max-1]
      its.each do |iteration|
        k_vlist.push @vlist[iteration]
      end
    end

    #k_vlist.each do |kvl|
    #  print "#{kvl}\n\n"
    #end

    k_vlist.each do |k_vl|
      path = [dst]
      vex = dst
      while vex != @source_node
        path.push k_vl[vex].prev
        vex = k_vl[vex].prev
      end
      path_list.push path.reverse
    end

    return path_list

  end

  private

  def max_of_k dst
    vmin(@vlist[0][dst].nbs.size,k)
  end

  def has_vex vname
    return @vlist[0][vname]
  end

  def vex_hash_clone vex_hash
    vl = {}
    vex_hash.each do |key, djvex|
      vl[key] = djvex.clone
    end
    return vl
  end

  def start_structures links, src
    @vlist  = [{}]
    @pqueue = {}
    @source_node = src
    @iterations = Hash.new { [].freeze }
    
    links.each do |l|
      if l.src == src
        pqueue[l.src] = 0.0
      else
        pqueue[l.src] = Float::INFINITY
      end

      if l.dst == src
        pqueue[l.dst] = 0.0
      else
        pqueue[l.dst] = Float::INFINITY
      end

      if has_vex l.src
        @vlist[0][l.src].nbs.push l
      else
        if l.src == src
          @vlist[0][l.src] = Dijkstra_vex.new :source
          @vlist[0][l.src].nbs.push l
        else
          @vlist[0][l.src] = Dijkstra_vex.new :any
          @vlist[0][l.src].nbs.push l
        end
      end
    
      if has_vex l.dst
        @vlist[0][l.dst].nbs.push l
      else
        if l.dst == src
          @vlist[0][l.dst] = Dijkstra_vex.new :source
          @vlist[0][l.dst].nbs.push l
        else
          @vlist[0][l.dst] = Dijkstra_vex.new :any
          @vlist[0][l.dst].nbs.push l
        end
      end
    end
  end

end
