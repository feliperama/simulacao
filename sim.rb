require "./lib/topology.rb"
require "set"

##### Initial Parameters

if ARGV.size != 4
  abort("ERROR: Wrong number of parameters. Example:\n $ ruby sim.rb all.txt demanda_2015.txt 14 2")
end

file_fds = ARGV[0]
file_dm = ARGV[1]
nregens = ARGV[2].to_i
regens_alg = ARGV[3].to_i

file_links = 'links_mapa.txt'

min_w = 500
max_w = 1400


#########DA Planilha
# interfaces 100G para trabalho e protecao

wds = [  571,
        614,
        754,
        587,
        624,
        744,
        1744,
        1733,
        1285,
        1295,
        1758,
        1731,
        1319,
        1269,
        952,
        519,
        489,
        948,
        459,
        494,
        929,
        3619,
        1560,
        3656,
        948,
        3609,
        1546,
        3632,
        3068,
        3083,
        509,
        483,
        2106,
        2096 ]

pds = [  4053,
        4516,
        4037,
        4540,
        534,
        560]

wds_reg = 0
wds.each do |wd|
    wds_reg += wd / max_w
end

pds_reg = 0
pds.each do |pd|
    pds_reg +=  pd / max_w
end

total_reg = wds_reg + pds_reg
if_add_drop = (wds.size + pds.size) * 2
if_w_p = total_reg * 2 + if_add_drop

print "Total de interfaces 100G rede utilizadas apenas para trabalho e protecao: #{ if_w_p  }\n"


#########################################################
#### lOADING DOUBLE FAIL AND RESTORATION INFORMATION ####
#########################################################

##Expressoes regulares utilizadas para ler os arquivos
regex_fd = /^[a-z]++\-+[a-z]*/i          #falha dupla
regex_d = /\A[\d]+$/                     #digito (numero)
regex_bl = /^\s*(#|$)/                   #Blank line

regex_flow_ok = /^\d*+\s+\-$/  #ex.: "22 -"
regex_rflow = /^\d*+\s+[\w]+\-+[\w*]+/i #ex.: "29 RC-EBVG,SDR-BRRR,BA-ITB,VTA-ROS,RJO-BOT,RJO-ARC"
regex_noflow = /^\-+\s+\-$/  #ex.:"- -"


##Abrindo arquivo das demandas
dm = Hash.new
File.open(file_dm, 'r') do |f|
  f.each_line do |line|
    str = line.to_s
    str = str.tr("\n", "")
    str = str.split(" ")
    dm[str[0].to_i] = str[1].to_i
  end
end


##Abrindo arquivo dos links com as distancias
links_weight = Array.new
links_set = Set.new 
File.open(file_links, 'r') do |f|
  f.each_line do |line|
    str = line.to_s
    str = str.tr("\n", "")
    str = str.split(" ")
    l = Link.new str[0], str[1], str[2].to_i
    links_weight.push l 
    links_set.add str[0]
    links_set.add str[1]
  end
end

##funcao para achar o peso de um link. MUDAR: Criar uma classe para isso e o anterior
def find_weight v1, v2, links_w
  l1 = Link.new v1, v2, 0
  links_w.each do |l|
    if l1==l
      return l.weight
    end
  end
end


##abrindo arquivo das falhas duplas e rotas de restauracao
fds = Array.new
File.open(file_fds, 'r') do |f|
  f.each_line do |line|
    str = line.to_s
    str = str.tr("\n", "")
    if line.match regex_fd
      str = str.split(',')
#      print "#{ find_weight str[0], str[1], links_weight } "
      l1 = Link.new str[0], str[1], (find_weight str[0], str[1], links_weight)
      l2 = Link.new str[2], str[3], (find_weight str[2], str[3], links_weight)
      fds.push DoubleFault.new l1, l2
    end
    
    if line.match regex_rflow
      str = str.split(/\s/)
      str_vertices = str[1].split(',')
      links = Array.new
      for i in 0..(str_vertices.size-2)
        w = find_weight str_vertices[i], str_vertices[i+1], links_weight
        links.push Link.new str_vertices[i], str_vertices[i+1], w
      end
      fds.last.add_rflow str[0].to_i, links, str_vertices
    end  
  end
end

##########################################
#### LINKS COMPARTILHADOS - ALL REGEN ####
##########################################

ll = Link_list_counter.new
ll.calculate fds, 100, dm
print "Regenerando em todos os sites - numero de interfaces 100G: #{ll.interfaces + if_w_p} \n"

###################################
#### ENCONTRANDO REGENERADORES ####
###################################

##Heuristica 1
##Verificando os vertices que mais ocorrem nas restauracoes

vcount = {}
fds.each do |fd|
  fd.flows_v.each do |flow_id, rpath_v|
    rpath_v.each do |v|
      if vcount[v]==nil
        vcount[v] = 1
      else
        vcount[v] = vcount[v] + 1
      end 
    end
  end
end

##Utilizando nregens* vertices mais ocorridos
vcount = vcount.sort_by { |v, n| n }
vcount_stripped = vcount[(nregens*-1)..-1]
vregens = []
vcount_stripped = vcount_stripped.each { |x| vregens.push x[0] }


##########################################
##### Heuristica 2 escolha dos regens ####
##########################################

groups = Array.new
fds.each do |fd|
  fd.flows.each do |flow_id, rpath|
    w = rpath[0].weight
    rpath_groups = Set.new
    prev_v = rpath[0].src 
    for i in 1..rpath.size-1
      if w < min_w
        w += rpath[i].weight
        prev_v = rpath[i].src 
      elsif ( w > min_w ) && ( w < max_w) 
        w += rpath[i].weight
        rpath_groups.add rpath[i].src.clone
        prev_v = rpath[i].src 
      elsif w > max_w
        if rpath_groups.empty?      #VERIFICAR SE DEU CERTO?
          rpath_groups.add prev_v.clone
        end
        groups.push rpath_groups
        rpath_groups = Set.new
        w = rpath[i].weight
      else
      end 
    end
  end
end

## Contando quantas vezes os vertices aparecem nos grupos
vertices = Set.new
fds.each do |fd|
  fd.flows_v.each do |flow_id, rpath_v|
    rpath_v.each do |v|
      vertices.add v
    end
  end
end
vertices = vertices.to_a
h_vertices = Hash.new
vertices.each do |v|
  count = 0
  groups.each do |group|
    if group.include? v
      count += 1
    end
  end
  h_vertices[v] = count
end
#print "#{ h_vertices }\n\n"

##Verificando dentro dos grupos qual será o vertice mais utilizado

vregens2 = Set.new
groups.each do |group|
  vname = String.new
  vtimes = 0
  group.to_a.each do |v|
    if h_vertices[v] > vtimes
      vname = v
      vtimes = h_vertices[v]
    end
  end
  vregens2.add vname
end
vregens2 = vregens2.to_a

##################################
#### OTIMIZACAO REGENERADORES ####
##################################

if regens_alg == 0
  rgs = Regenerators.new
elsif regens_alg == 1
  rgs = Regenerators.new vregens
elsif regens_alg == 2
  rgs = Regenerators.new vregens2
end 
  
puts "otimizacao - numero de sites regeneracao: #{ rgs.regs.size }"  
print "Sites de regeneracao: #{ rgs } \n"

opt_fds = Array.new  
max_weight = 0
max_weight_id = Array.new
bad_links = Hash.new
fds.each_with_index do |fd, fd_index|
  opt_fds.push DoubleFault.new(fd.link1.clone, fd.link2.clone)

  fd.flows.each do |flow_id, rpath|
    str_vertices = Array.new
    str_vertices_weight = Array.new
    new_rpath = Array.new
    
    str_vertices.push rpath[0].src
    w = rpath[0].weight
    prev_v = rpath[0].src.clone
    next_v = ""
    for i in 1..(rpath.size-1)
      if rgs.is_reg(rpath[i].src)
        str_vertices.push rpath[i].src
        str_vertices_weight.push w
        if max_weight < w
          max_weight = w
          max_weight_id = [ fd_index, flow_id ]
        end
        if w > 1100
          next_v = rpath[i].src.clone
          bad_links[ (prev_v << ":" << next_v)  ] = w
        end

        w = rpath[i].weight
      else
        w += rpath[i].weight
      end
    end
    str_vertices.push rpath.last.dst

    for i in 0..str_vertices.size-2
      new_rpath.push Link.new(str_vertices[i], str_vertices[i+1], str_vertices_weight[i])
    end
    
    opt_fds.last.add_rflow flow_id, new_rpath, str_vertices

  end
end

print "Maxima distancia sem regeneracao: #{ max_weight } \n"
print "  Rota com maior distancia sem regeneracao: #{ max_weight_id }\n"
print "Links com distancia > #{ max_w }: #{ bad_links.size } \n"

#LINKS COMPARTILHADOS - SITES ESPECIFICOS DE REGENERACAO

opt_ll = Link_list_counter.new
opt_ll.calculate opt_fds, 100, dm
print "Total de interfaces 100G - otimizado: #{opt_ll.interfaces + if_w_p } \n"
print "Interfaces 100G por site - apenas regeneracao#{ opt_ll.v_ifs.to_s  } \n\n"


